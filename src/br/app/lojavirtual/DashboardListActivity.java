package br.app.lojavirtual;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class DashboardListActivity extends Activity {
	
	private ListView listDash;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard_list);
		
		listDash = (ListView) findViewById(R.id.listDash);
		
		List<CharSequence> lista = new ArrayList<CharSequence>();
		lista.add("123");
		lista.add("123");
		lista.add("123");
		
		ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(DashboardListActivity.this, 
												android.R.layout.simple_list_item_1, lista);
		
		listDash.setAdapter(adapter);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 1, 1, "Menu");
		return true;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		int id = item.getItemId();
		if(id == 1){
			startActivity(new Intent(DashboardListActivity.this, MainActivity.class));
			finish();
		}
		return true;
	}
}

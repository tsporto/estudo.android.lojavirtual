package br.app.lojavirtual.util;

import br.app.lojavirtual.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface.OnClickListener;
import android.widget.Toast;

public class Mensagem {

	public static void addMsg(Activity activity, String msg){
		Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
	}
	
	public static void addMsgOK(Activity activity, String titulo, String msg, int icone){
		AlertDialog.Builder alert = new AlertDialog.Builder(activity);
		alert.setTitle(titulo);
		alert.setMessage(msg);
		alert.setNeutralButton("OK", null);
		alert.setIcon(icone);
		alert.show();
	}
	
	public static void addMsgConfirm(Activity activity, String titulo, String msg, int icone, OnClickListener listener){
		AlertDialog.Builder alert = new AlertDialog.Builder(activity);
		alert.setTitle(titulo);
		alert.setMessage(msg);
		alert.setPositiveButton(R.string.msg_sim, listener);
		alert.setNegativeButton(R.string.msg_nao, null);
		alert.setIcon(icone);
		alert.show();
	}
}

package br.app.lojavirtual;

import br.app.lojavirtual.util.Mensagem;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;


public class LoginActivity extends Activity {
	
	private EditText edtLogin, edtSenha;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		edtLogin = (EditText) findViewById(R.id.edtLogin);
		edtSenha = (EditText) findViewById(R.id.edtSenha);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0,1,1,"Sair");
		menu.add(0,2,2,"Info");
		return true;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		int idmenu = item.getItemId();
		
		switch (idmenu) {
			case 1:
				Mensagem.addMsgConfirm(LoginActivity.this, "Sair", "Deseja sair?", R.drawable.ic_action_remove, new OnClickListener() {					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();						
					}
				});				
				break;
			case 2:
				Mensagem.addMsgOK(LoginActivity.this, "Sobre", "Copyright 2014 | WaibTec\nTodos os direitos reservados", R.drawable.ic_action_about);				
				break;
		}
		
		return true;
	}
	
	public void logar(View v){
		String login = edtLogin.getText().toString();
		String senha = edtSenha.getText().toString();
		
		boolean validacao = true;
		
		if(login == null || login.equals("")){
			validacao = false;
			edtLogin.setError(getString(R.string.msg_login_obg));
		}
		
		if(senha == null || senha.equals("")){
			validacao = false;
			edtSenha.setError(getString(R.string.msg_senha_obg));
		}
		
		if(validacao){
			startActivity(new Intent(this, MainActivity.class));		
			finish();
		}
	}
}

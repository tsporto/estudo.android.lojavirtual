package br.app.lojavirtual;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 1, 1, "Lista");
		return true;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		int id = item.getItemId();
		if(id == 1){
			startActivity(new Intent(MainActivity.this, DashboardListActivity.class));
			finish();
		}
		return true;
	}
}
